'use strict';

const Route = use('Route');

Route.group(() => {
  Route.get('/', ({ request }) => {
    return { hello: 'world' };
  });
}).prefix('api/v1');

// Restricted routes.
Route.group(() => {
  Route.get('/restricted', ({ request }) => {
    return { world: 'hello' };
  });
})
  .prefix('api/v1')
  .middleware('auth:jwt');
